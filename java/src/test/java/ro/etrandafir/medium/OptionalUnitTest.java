package ro.etrandafir.medium;

import java.util.Optional;

import lombok.Getter;

public class OptionalUnitTest {

    @Getter
    class Account {
        boolean isActive;
        Membership membership;

        Optional<Membership> getOptionalMembership() {
            return Optional.ofNullable(membership);
        }
    }

    @Getter
    class Membership {
        Object membershipType;
    }

    class AccountRepository {
        public Account findByUsername(String username) {
            return null;
        }

        public Optional<Account> findByEmail(String email) {
            return Optional.empty();
        }
    }

    class AccountNotEligeble extends RuntimeException {}


    AccountRepository accountRepository = new AccountRepository();


    public Account ex1() {
        Account account = accountRepository.findByUsername("asd");
        if(account == null) {
            throw new AccountNotEligeble();
        }
        return account;
    }

    public Account ex1_opt() {
        return accountRepository.findByEmail("asd")
            .orElseThrow(AccountNotEligeble::new);
    }


    public Membership ex2() {
        Account account = accountRepository.findByUsername("asd");
        if(account == null || account.getMembership() == null) {
            throw new AccountNotEligeble();
        }
        return account.getMembership();
    }

    public Membership ex2_opt() {
        return accountRepository.findByEmail("asd")
            .flatMap(Account::getOptionalMembership)
            .orElseThrow(AccountNotEligeble::new);
    }

    public Membership ex3() {
        Account account = accountRepository.findByUsername("asd");
        if(account == null || account.getMembership() == null || !account.isActive()) {
            throw new AccountNotEligeble();
        }
        return account.getMembership();
    }

    public Membership ex3_opt() {
        return accountRepository.findByEmail("asd")
            .filter(Account::isActive)
            .flatMap(Account::getOptionalMembership)
            .orElseThrow(AccountNotEligeble::new);
    }

}
